if (Drupal.jsEnabled) {
  $(document).ready(function() {
		if ($("#edit-project-name").val() == 'other') {
			$("#other_project").show();
		} else {
			$("#other_project").hide();
		}
		
		$("#edit-project-name").change(function() {
			if ($(this).val() == 'other') {
				$("#other_project").show();
			} else {
				$("#other_project").hide();
			}
		});
  });
}